import javafx.scene.control.Button;

public class KeyBoardButton extends Button {

	private int buttonNumber = 0;
	private char buttonChar = ' ';
	public boolean isUsed = false;
	
	public KeyBoardButton(char c) {
		this.buttonChar = c;
		this.buttonNumber = ((int) c) - 65;
	}
	
	public void setLayoutPosition(int x, int y) {
		this.setLayoutX(x);
		this.setLayoutY(y);
	}
	
	public void setLayoutPositionWithText(int x, int y, String text) {
		this.setLayoutPosition(x, y);
		this.setText(text);
	}
	
	public int getButtonNumber() {
		return this.buttonNumber;
	}
	
	public char getButtonCharactere() {
		return this.buttonChar;
	}

	public void disableButton(boolean value) {
		this.setDisable(value);
	}
	
	
	
}
