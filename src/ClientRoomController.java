import java.util.ArrayList;

/*
 * Permet de sauvegarder les Rooms au niveau de client. 
 */

public class ClientRoomController {
    private ArrayList<ClientRoom> clientRooms = new ArrayList<ClientRoom>();

    public ClientRoomController(){

        addRoom(1, 1);
        addRoom(2, 1);
        addRoom(3, 0);
        addRoom(4, 2);
        addRoom(5, 0);
        addRoom(6, 1);
        getAllRoomsFromServers();
    }

    public void getAllRoomsFromServers(){
        /**
         * Ajouter la récupérations de rooms
         */
    }

    public ArrayList<ClientRoom> getClientRooms(){
        return this.clientRooms;
    }

    public int numberOfRooms(){
        return this.clientRooms.size();
    }

    public void addRoom(ClientRoom cr){
        clientRooms.add(cr);
    }

    public void addRoom(int id, int members){
        clientRooms.add(new ClientRoom(id, members));
    }
    
    public void connectToRoom(ClientRoom room) {
    	room.addMember();
    	ClientRoom roomBis = getRoom(room.getId());
    	roomBis.addMember();
    }

    public ClientRoom getRoom(int id){
        ClientRoom room = null;
        for(ClientRoom cR : clientRooms){
            if(cR.isRoom(id)){
                room = cR;
            }
        }
        return room;
    }
   
}

class ClientRoom{
    private int id;
    private int members;
    private int membersMax = 2;

    public ClientRoom(int id, int members){
        addMembers(members);
        setId(id);
    }

    public ClientRoom(int id, int members, int membersMax){
        addMembers(members);
        setId(id);
        this.membersMax = membersMax;
    }

    public void addMembers(int count){
        if(count < (membersMax - members)){
            members += count;
        }
    }
    
    public void addMember(){
    	members += 1;
    }

    public void removeMembers(int count){
        if(count > (membersMax - members)){
            members -= count;
        }
    }

    public void setId(int id){
        this.id = id;
    }

    public int getId(){
        return this.id;
    }

    public int getMembers(){
        return this.members;
    }

    public int getMembersMax(){
        return this.membersMax;
    }

    public boolean isRoom(int id){
        return id == this.id;
    }

}